#!/bin/bash

set -u # stop if a variable is not initialized
set -e # stop in case of error

for i in files/*yml; do
  echo "Linting ${i}..."
  pykwalify -d $i -s ./.schema.yaml
done
