import argparse
import collections
import sys
import xml.etree.ElementTree as ElementTree
from yaml import safe_load, YAMLError


def filter_sort_xml_on_yaml(tools_xml, tools_yaml):
    tools_sorted = []
    for id in tools_yaml:
        if id in tools_xml:
            tools_sorted.extend(tools_xml[id])
        else:
            print("This tool isn't installed %s" % id)
    return tools_sorted

def filter_xml_not_in_yaml(tools_xml, yaml_ids):
    tools_xml_filtered = []
    for id in tools_xml:
        if id not in yaml_ids:
            tools_xml_filtered.extend(tools_xml[id])
    return tools_xml_filtered

def main(args):
    print('Reading shed_tool_conf.xml ...')
    tree = ElementTree.parse(args.shed_tool_conf.name)
    toolbox = tree.getroot()
    tool_path = toolbox.get("tool_path")

    tools_xml = collections.OrderedDict()
    for section in toolbox.findall('section'):
        repository_name_ref = ""
        for tool in section.findall('tool'):
            repository_name = tool.find('repository_name').text
            repository_owner = tool.find('repository_owner').text

            # Check if there are several tools in the same section
            if repository_name_ref != "" and repository_name_ref != repository_name:
                repository_name_ref = repository_name
                sys.stderr.write("There are at least 2 different tools in this section: %s and %s\n" % (repository_name_ref, repository_name))
                sys.exit(1)
            repository_name_ref = repository_name

            id = repository_name+"---"+repository_owner
            if (id not in tools_xml.keys()):
                tools_xml[id] = []
            tools_xml[id].append(section)
            break


    tools_sorted = {}
    yaml_ids = []
    for yaml in args.yaml:
        print('Reading %s ...' % yaml.name)
        with open(yaml.name, 'r') as stream:
            try:
                tool_list  = safe_load(stream)
            except YAMLError as exc:
                print(exc)

        tools_yaml = collections.OrderedDict()
        for tool_yaml in tool_list['tools']:
            id = tool_yaml['name']+"---"+tool_yaml['owner']
            yaml_ids.append(id)
            tools_yaml[id] = tool_yaml

        tools_sorted[yaml.name] = filter_sort_xml_on_yaml(tools_xml, tools_yaml)

        # for section in tools_sorted[yaml.name]:
        #     for tool in section.findall('tool'):
        #         repository_name = tool.find('repository_name').text
        #         print(repository_name)

    tools_xml_filtered = filter_xml_not_in_yaml(tools_xml, yaml_ids)

    # for section in tools_xml_filtered:
    #      for tool in section.findall('tool'):
    #          repository_name = tool.find('repository_name').text
    #          file = tool.get('file')
    #          if (file == "toolshed.g2.bx.psu.edu/repos/devteam/emboss_5/dc492eb6a4fc/emboss_5/emboss_polydot.xml"):
    #              print(file)

    print('XML Building ...')
    toolbox_new = ElementTree.Element('toolbox')
    toolbox_new.set('tool_path',tool_path)
    for yaml in args.yaml:
        print('\tImporting from %s ...' % yaml.name)
        for section in tools_sorted[yaml.name]:
            toolbox_new.append(section)
    print('\tImporting the other tools ...')
    for section in tools_xml_filtered:
        toolbox_new.append(section)

    print('XML Writing ...')
    xml_string = ElementTree.tostring(toolbox_new).decode()
    output_file = open(args.shed_tool_conf.name+".new.xml", "w")
    output_file.write(xml_string)
    output_file.close()

    print('Cheking ...')
    print(args.shed_tool_conf.name+".new.xml")
    tree_new_imported = ElementTree.parse(args.shed_tool_conf.name+".new.xml")
    toolbox_new_imported = tree_new_imported.getroot()
    if len(toolbox_new_imported.findall('section/tool')) != len(toolbox.findall('section/tool')):
        sys.stderr.write("The number of tools in the two shed_tool_conf didn't match - org:%d != new:%d \n" % (len(toolbox_new_imported.findall('section/tool')), len(toolbox.findall('section/tool'))))
        sys.exit(2)


if __name__ == "__main__":
    # execute only if run as a script

    parser = argparse.ArgumentParser(description='This tool will reorder the Galaxy shed_tool_conf.xml according to yaml tool list')
    parser.add_argument('--shed_tool_conf', type=argparse.FileType('r'), required=True, help="shed_tool_conf.xml")
    parser.add_argument('--yaml', type=argparse.FileType('r'), nargs='+', required=True, help="tool.yaml file[s]")
    args = parser.parse_args()

    main(args)
